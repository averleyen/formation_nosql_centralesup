###INSTALLATION Docker et (docker-compose)###
#Linux#
Télécharger et installez : https://store.docker.com/search?type=edition&offering=community (ou via un apt-get pour les Ubuntu Users)

#Windows / Mac OS#
Télécharger et installez Docker Toolbox: https://www.docker.com/products/docker-toolbox 

Lors de l’installation, choisissez les options
•	Docker compose
•	Kitematic 
•	Git

Lancez Kitematic
Info Windows Users : sur les licences en dessous de Professionnel, vous ne pourrez pas utiliser le système Hyper-V, il vous proposera de faire une image sur VM … surtout n’hésitez pas à valider 😊
!!!Pas besoin de vous créer un compte Docker Hub pour le moment!!!

###INSTALLATION Robomongo###
Téléchargez et installez RoboMongo : https://robomongo.org/download

###Lancement des dockers###
#Windows et Mac# : 
Lancez Kitematic lorsque la fenêtre s'ouvre cliquez sur le bouton docker cli (en bas à gauche)
Placez vous dans le dossier formation_nosql_centralesup (syncrhronisé via git) 
Lancez un docker-compose.exe build 
Lancez un docker-compose.exe up 

#Linux# 
Ouvrez un shell 
Placez vous dans le dossier formation_nosql_centralesup (syncrhronisé via git) 
Lancez un docker-composer build
Lancez un docker-composer up

###TESTS DE FONCTIONNEMENT###
[HOST] = 192.168.99.100 (Windows) ou 0.0.0.0 (Linux, Mac, Windows Pro)

###TESTS DE FONCTIONNEMENT###

#Serveur PHP#
[HOST] => Réponse Accueil IHM 

#MySQL + PMA#
[HOST]:8080/ => Accueil PHP My Admin 
Serveur 192.168.99.100
Login : root 
Password : Admin 

=> Accès à la BDD MySQL

#Neo4j#
[HOST]:7474/ => Accueil Neo4j
Login : neo4j 
Password : neo4j 

=> Puis choisissez un nouveau password 

#MongoDB#
Depuis Robomongo
[HOST]:27017/
Lancez une connection. 